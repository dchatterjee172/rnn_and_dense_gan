import tensorflow as tf
from tqdm import tqdm


def train(generator, discriminator, train_data, test_data, epoch, num_classes):
    optimizer = tf.keras.optimizers.Adam()
    train_discriminator_loss_original = tf.keras.metrics.Mean(
        name="train_discriminator_loss_original"
    )
    discriminator_loss_generated = tf.keras.metrics.Mean(
        name="discriminator_loss_generated"
    )
    generator_loss_generated = tf.keras.metrics.Mean(name="generator_loss_generated")
    train_discriminator_acc_original = tf.keras.metrics.Accuracy(
        name="train_discriminator_acc_original"
    )
    test_discriminator_loss_original = tf.keras.metrics.Mean(
        name="test_discriminator_loss_original"
    )
    test_discriminator_acc_original = tf.keras.metrics.Accuracy(
        name="test_discriminator_acc_original"
    )
    metrics = (
        train_discriminator_loss_original,
        discriminator_loss_generated,
        generator_loss_generated,
        train_discriminator_acc_original,
        test_discriminator_loss_original,
        test_discriminator_acc_original,
    )

    @tf.function
    def train_step_generator(num_generated):
        generated_labels_generator = tf.random.uniform(
            shape=(num_generated,), minval=0, maxval=num_classes - 1, dtype=tf.int32
        )
        generated_labels_one_hot_generator = tf.one_hot(
            generated_labels_generator, depth=num_classes
        )
        random = tf.random.normal(
            shape=(num_generated, num_classes - 1),
            mean=generated_labels_one_hot_generator[..., :-1],
            stddev=tf.ones(shape=(num_generated, num_classes - 1)) * 0.0001,
        )
        with tf.GradientTape() as tape:
            generated_images = generator(random)
            logits = discriminator(generated_images)
            loss = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(
                    labels=generated_labels_one_hot_generator, logits=logits
                )
            )
        gradients = tape.gradient(loss, generator.trainable_variables)
        gradients, grad_norm = tf.clip_by_global_norm(gradients, 1)
        optimizer.apply_gradients(zip(gradients, generator.trainable_variables))
        generator_loss_generated(loss)
        return tf.expand_dims(generated_images, -1), tf.argmax(logits, -1), grad_norm

    @tf.function
    def train_step_discriminator(images, labels, num_generated):
        original_labels = labels
        original_labels_one_hot = tf.one_hot(original_labels, depth=num_classes)
        generated_labels_discriminator = (
            tf.cast(tf.fill(dims=(num_generated,), value=num_classes - 1), tf.uint8),
        )
        generated_labels_one_hot_discriminator = tf.one_hot(
            generated_labels_discriminator, depth=num_classes
        )
        random = tf.random.normal(
            shape=(num_generated, num_classes - 1),
            mean=tf.one_hot(
                tf.random.uniform(
                    shape=(num_generated,),
                    minval=0,
                    maxval=num_classes - 1,
                    dtype=tf.int32,
                ),
                depth=num_classes - 1,
            ),
            stddev=tf.ones(shape=(num_generated, num_classes - 1)) * 0.0001,
        )
        with tf.GradientTape() as tape:
            generated_images = tf.stop_gradient(generator(random))
            images = tf.concat((generated_images, images), 0)
            logits = discriminator(images)
            original_logits, generated_logits = (
                logits[num_generated:, ...],
                logits[:num_generated, ...],
            )
            loss_original = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(
                    labels=original_labels_one_hot, logits=original_logits
                )
            )
            loss_generated = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(
                    labels=generated_labels_one_hot_discriminator,
                    logits=generated_logits,
                )
            )
            loss_total = loss_original + loss_generated
        gradients = tape.gradient(loss_total, discriminator.trainable_variables)
        gradients, grad_norm = tf.clip_by_global_norm(gradients, 1)
        optimizer.apply_gradients(zip(gradients, discriminator.trainable_variables))
        train_discriminator_acc_original(
            original_labels, tf.argmax(original_logits, -1)
        )
        train_discriminator_loss_original(loss_original)
        discriminator_loss_generated(loss_generated)
        return grad_norm

    @tf.function
    def pred_step(images):
        logits = discriminator(images)
        return tf.argmax(logits, -1), logits

    @tf.function
    def test_step(images, labels):
        pred, logits = pred_step(images)
        labels_one_hot = tf.one_hot(labels, depth=num_classes)
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
                labels=labels_one_hot, logits=logits
            )
        )
        test_discriminator_loss_original(loss)
        test_discriminator_acc_original(labels, pred)

    for i in tqdm(range(epoch)):
        for metric in metrics:
            metric.reset_states()
        for images, labels in tqdm(train_data, desc="training discriminator"):
            grad_norm = train_step_discriminator(images, labels, num_generated=2)

        tf.summary.scalar("dis_grad", grad_norm, step=i)
        for _ in tqdm(range(2000), desc="training generator"):
            generated_images, generated_labels, grad_norm = train_step_generator(
                num_generated=30
            )
        tf.summary.scalar("gen_grad", grad_norm, step=i)
        tf.summary.image("generated", generated_images, step=i)
        tf.summary.histogram("generated_labels", generated_labels, step=i)
        for images, labels in tqdm(
            test_data, desc="testing discriminator on original data"
        ):
            test_step(images, labels)
        for metric in metrics:
            tf.summary.scalar(metric.name, metric.result(), step=i)
