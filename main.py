import tensorflow as tf
import argparse
from model import Generator, Discriminator
from train import train
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("model_dir", type=str)
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--epoch", type=int, default=32)
    args = parser.parse_args()
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.fashion_mnist.load_data()
    x_train = (x_train.astype(np.float32) - 128) / 128
    x_test = (x_test.astype(np.float32) - 128) / 128
    train_data = (
        tf.data.Dataset.from_tensor_slices((x_train, y_train))
        .shuffle(10000)
        .batch(args.batch_size)
    )

    test_data = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(
        args.batch_size
    )
    summary_writer = tf.summary.create_file_writer(args.model_dir)
    generator = Generator(output_shape_=(28, 28))
    discriminator = Discriminator(num_classes=11)
    with summary_writer.as_default():
        train(
            generator=generator,
            discriminator=discriminator,
            train_data=train_data,
            test_data=test_data,
            epoch=args.epoch,
            num_classes=11,
        )
