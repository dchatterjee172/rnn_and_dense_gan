import tensorflow as tf

layers = tf.keras.layers


class Discriminator(tf.keras.Model):
    def __init__(self, num_classes):
        super(Discriminator, self).__init__()
        self.horizontal = layers.Bidirectional(
            layers.GRU(
                10, return_sequences=False, unroll=True, recurrent_activation=tf.nn.tanh
            )
        )
        self.vertical = layers.Bidirectional(
            layers.GRU(
                10, return_sequences=False, unroll=True, recurrent_activation=tf.nn.tanh
            )
        )
        self.dense = layers.Dense(num_classes)

    def call(self, x):
        vertical = self.vertical(x)
        horizontal = self.horizontal(tf.transpose(x, (0, 2, 1)))
        return self.dense(tf.concat((vertical, horizontal), -1))


class Generator(tf.keras.Model):
    def __init__(self, output_shape_):
        super(Generator, self).__init__()
        self.dense_1 = layers.Dense(200)
        self.dense_2 = layers.Dense(output_shape_[0] * output_shape_[1])
        self.output_shape_ = output_shape_

    def call(self, x):
        x = tf.nn.tanh(2 / 3 * self.dense_1(x)) * 1.714
        x = tf.nn.tanh(self.dense_2(x))
        return tf.reshape(x, (-1, *self.output_shape_))
